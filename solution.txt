Using Docker to simplify development Overview

Register a gitlab-runner build agent(AWS EC2) via Terraform, and install the necessary dependencies via terraform remote-exec.
In CI phase, run compile, test, package and generate the docker image, then push to container registry
In CD phase, the gitlab-runner(EC2 VM) pull from the container registry the newly genrated docker image(From CI phase) and deploy the application using the docker image via docker-compose.
The application could be viewed at http://54.252.188.204:8081/services/UMSL (exposing the application port on 8081 by changing the yml file).


CI (When code changes on Dev Branch or Merge to Dev from feature branch)
Run:
    - build, maven-compile
    - test, maven-test
    - analyze, Code quality using Sonar
    - package, package to jar
    - push, push the new image to container hub registry.gitlab.com
Please refer to .gitlab-ci.yml for details.

CD(when merged to Master branch)
Run sudo docker-compose -f src/main/docker/app.yml up to deploy the application using the newly built image.

I never touched Jhipster/Java/Sonar dev before, so the pipeline probably needs a lot improvement.
