variable "private_key" {
  default = "svn_consultancy-syd-201505.pem"
}

variable "instance_type" {
  default = "t3.medium"
}


variable "key_name" {
  description = "Desired name of AWS key pair"
  default     = "svn_consultancy-syd-201505"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-southeast-2"
}

# Ubuntu Precise 12.04 LTS (x64)
variable "aws_amis" {
  default = {
    eu-west-1      = "ami-674cbc1e"
    ap-southeast-2 = "ami-0edcec072887c2caa"
  }
}

variable "s3_backend" {
  default = "servianterraformlabforelasticsearch"
}

variable "global_tags" {
  description = "Tags to be added to all resources"
  type        = "map"
  default = {
    ManagedBy = "yang lu"
    Client    = "N/A"
    Project   = "USSL"
  }
}

